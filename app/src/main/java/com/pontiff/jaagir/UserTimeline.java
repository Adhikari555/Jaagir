package com.pontiff.jaagir;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.firebase.client.core.Tag;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class UserTimeline extends AppCompatActivity {
    private static final String TAG = "UserTimeline";

//    String username;
 //   EditText useraddress;
 //   EditText userskills;
  //  EditText userphone;
  //  EditText usereducation;
  //  EditText usercategory;
   // EditText userexperience;

    Button logout;

    FirebaseAuth mAuth;

    DatabaseReference mRef;
    String userID;
    ListView listViewUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_timeline);

        listViewUser = (ListView) findViewById(R.id.listView);
        Firebase.setAndroidContext(this);
        mAuth = FirebaseAuth.getInstance();

        mRef = FirebaseDatabase.getInstance().getReference().child("UserList");
        FirebaseUser user = mAuth.getCurrentUser();
        userID = user.getUid();






        //  username = (TextView) findViewById(R.id.username1);
       // useraddress = (EditText) findViewById(R.id.useraddress1);
       // userskills = (EditText) findViewById(R.id.userskills1);
       // userphone = (EditText) findViewById(R.id.userphone1);
       // usercategory = (EditText) findViewById(R.id.category1);
        //userexperience = (EditText) findViewById(R.id.experience1);
        logout = (Button) findViewById(R.id.logout1);

       logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserTimeline.this, MainActivity.class);
                startActivity(intent);
            }
        });


        mRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                showData(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void showData(DataSnapshot dataSnapshot) {

        for(DataSnapshot ds : dataSnapshot.getChildren()){
            UserInformation uinfo = new UserInformation();
            uinfo.setName(ds.child(userID).getValue(UserInformation.class).getName());
            uinfo.setAddress(ds.child(userID).getValue(UserInformation.class).getAddress());
            uinfo.setSkills(ds.child(userID).getValue(UserInformation.class).getSkills());
            uinfo.setEducation(ds.child(userID).getValue(UserInformation.class).getEducation());
            uinfo.setExperience(ds.child(userID).getValue(UserInformation.class).getExperience());
            uinfo.setPhone(ds.child(userID).getValue(UserInformation.class).getPhone());
            uinfo.setCategory(ds.child(userID).getValue(UserInformation.class).getCategory());

            Log.d(TAG, "showData: name: "+ uinfo.getName());
            Log.d(TAG, "showData: address: "+ uinfo.getAddress());
            Log.d(TAG, "showData: skills: "+ uinfo.getSkills());
            Log.d(TAG, "showData: education: "+ uinfo.getEducation());
            Log.d(TAG, "showData: experience: "+ uinfo.getExperience());
            Log.d(TAG, "showData: phone: "+ uinfo.getPhone());
            Log.d(TAG, "showData: category: "+ uinfo.getCategory());

            ArrayList<String> array = new ArrayList<>();
            array.add(uinfo.getName());
            array.add(uinfo.getAddress());
            array.add(uinfo.getSkills());
            array.add(uinfo.getEducation());
            array.add(uinfo.getExperience());
            array.add(uinfo.getPhone());
            array.add(uinfo.getCategory());
            ArrayAdapter adapter = new ArrayAdapter(this,R.layout.activity_company_list, array);
            listViewUser.setAdapter(adapter);



        }
    }


    @Override
    protected void onStart() {
        super.onStart();

    }


}
