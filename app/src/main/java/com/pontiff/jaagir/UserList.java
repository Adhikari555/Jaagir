package com.pontiff.jaagir;

import java.util.List;

public class UserList {

    String userId;
    String userName;
    String userEducation;
    String userAddress;
    String userSkills;
    String userPhone;
    String userEmail;
    String userPassword;
    String userConfirmpassword;
    String userCategory;
    String userExperience;

    public UserList(UserTimeline userTimeline, List<UserList> userLists) {


    }

    public UserList(String userId, String userName, String userEducation, String userAddress, String userSkills, String userPhone, String userEmail, String userPassword, String userConfirmpassword, String userCategory,String userExperience) {
        this.userId = userId;
        this.userName = userName;
        this.userEducation = userEducation;
        this.userAddress = userAddress;
        this.userSkills = userSkills;
        this.userPhone = userPhone;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
        this.userConfirmpassword = userConfirmpassword;
        this.userCategory = userCategory;
        this.userExperience = userExperience;
    }


    public String getUserId() {
        return userId;
    }

    public String getUserName() {

        return userName;
    }

    public String getUserEducation()
    {
        return userEducation;
    }

    public String getUserAddress() {

        return userAddress;
    }

    public String getUserSkills()
    {
        return userSkills;
    }

    public String getUserPhone()
    {
        return userPhone;
    }

    public String getUserEmail() {

        return userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public String getUserConfirmpassword() {

        return userConfirmpassword;
    }

    public String getUserCategory(){
        return userCategory;
    }

    public String getUserExperience(){
        return userExperience;
    }
}