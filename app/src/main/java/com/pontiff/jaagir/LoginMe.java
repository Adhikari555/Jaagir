package com.pontiff.jaagir;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginMe extends AppCompatActivity {
    EditText uemail, upassword;
    Button ulogin;
    private FirebaseAuth mAuth;
    private DatabaseReference mRef;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_me);

        Firebase.setAndroidContext(this);
        mAuth = FirebaseAuth.getInstance();
        mRef = FirebaseDatabase.getInstance().getReference().child("UserList");
        uemail = (EditText) findViewById(R.id.emailme);
        upassword = (EditText) findViewById(R.id.passwordme);
        ulogin = (Button) findViewById(R.id.loginme2);


        ulogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showUser();
            }
        });


    }




    private void showUser() {
        String uemailValue = uemail.getText().toString().trim();
        String upasswordValue = upassword.getText().toString().trim();


        if (!TextUtils.isEmpty(uemailValue)&&!TextUtils.isEmpty(upasswordValue)){
            mAuth.signInWithEmailAndPassword(uemailValue, upasswordValue).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        checkUserExist();
                    } else {
                        Toast.makeText(LoginMe.this, "Error Login", Toast.LENGTH_LONG).show();
                    }
                }
            });

        }

    }

    private void checkUserExist() {
        final String user_id = mAuth.getCurrentUser().getUid();
        mRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(user_id)) {
                    Intent intent = new Intent(LoginMe.this, UserTimeline.class);
                    startActivity(intent);

                } else {
                    Toast.makeText(LoginMe.this, "Error hai", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}

