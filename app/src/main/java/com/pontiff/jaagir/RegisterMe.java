package com.pontiff.jaagir;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterMe extends AppCompatActivity {

    EditText UserName,UserAddress,UserSkills,UserPhone,UserEmail,UserPassword,UserConfirmPassword;
    Button UserRegister;
    Spinner UserSpinner,UserSpinner1,UserSpinner2;
    private DatabaseReference mRef;
    private FirebaseAuth mAuth;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_me);
        Firebase.setAndroidContext(this);

        mAuth = FirebaseAuth.getInstance();
        mRef = FirebaseDatabase.getInstance().getReference().child("UserList");

        UserName = (EditText) findViewById(R.id.username);
        UserSpinner = (Spinner) findViewById(R.id.userspinner);
        UserAddress =(EditText) findViewById(R.id.useraddress);
        UserSkills = (EditText) findViewById(R.id.userskills);
        UserPhone = (EditText) findViewById(R.id.userphone);
        UserEmail = (EditText) findViewById(R.id.useremail);
        UserPassword = (EditText) findViewById(R.id.userpassword);
        UserConfirmPassword = (EditText) findViewById(R.id.userconfirmpassword);
        UserSpinner2 = (Spinner) findViewById(R.id.userspinner2);
         UserSpinner1 = (Spinner) findViewById(R.id.userspinner1);
        UserRegister = (Button) findViewById(R.id.userregister);

         UserRegister.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 addUserList();


             }
         });


  }
    private void addUserList(){
        final String uname = UserName.getText().toString().trim();
        final String uspinner = UserSpinner.getSelectedItem().toString();
        final String uaddress = UserAddress.getText().toString().trim();
        final String uskills = UserSkills.getText().toString().trim();
        final String uphone = UserPhone.getText().toString().trim();
        final String uemail = UserEmail.getText().toString().trim();
        final String upassword = UserPassword.getText().toString().trim();
        final String uconfirmpassword = UserConfirmPassword.getText().toString().trim();
        final String uspinner2 = UserSpinner2.getSelectedItem().toString();
        final String uspinner1 = UserSpinner1.getSelectedItem().toString();




        mAuth.createUserWithEmailAndPassword(uemail,upassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()){
                    String user_id = mAuth.getCurrentUser().getUid();
                    DatabaseReference current_user_db = mRef.child(user_id);
                    current_user_db.child("userName").setValue(uname);
                    current_user_db.child("userEducation").setValue(uspinner);
                    current_user_db.child("userAddress").setValue(uaddress);
                    current_user_db.child("userEmail").setValue(uemail);
                    current_user_db.child("userPhone").setValue(uphone);
                    current_user_db.child("userPassword").setValue(upassword);
                    current_user_db.child("userConfirmPassword").setValue(uconfirmpassword);
                    current_user_db.child("userCategory").setValue(uspinner2);
                    current_user_db.child("userSkills").setValue(uskills);
                    current_user_db.child("userExperience").setValue(uspinner1);

                    Intent intent = new Intent(RegisterMe.this, UserTimeline.class);
                    startActivity(intent);
                }
            }
        });



        Toast.makeText(this,"user added" , Toast.LENGTH_LONG).show();



    }
}
