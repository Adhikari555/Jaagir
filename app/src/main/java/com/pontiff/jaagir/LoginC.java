package com.pontiff.jaagir;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class LoginC extends AppCompatActivity {
    EditText cemail, cpassword;
    Button clogin;
    private FirebaseAuth mAuth;
    private DatabaseReference mRef;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_c);

        Firebase.setAndroidContext(this);
        mAuth = FirebaseAuth.getInstance();
        mRef = FirebaseDatabase.getInstance().getReference().child("CompanyList");
        cemail = (EditText) findViewById(R.id.cemail);
        cpassword = (EditText) findViewById(R.id.cpassword);
        clogin = (Button) findViewById(R.id.clogin);


        clogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCompany();
            }
        });


    }




    private void showCompany() {
        String cemailValue = cemail.getText().toString().trim();
        String cpasswordValue = cpassword.getText().toString().trim();


     if (!TextUtils.isEmpty(cemailValue)&&!TextUtils.isEmpty(cpasswordValue)){
         mAuth.signInWithEmailAndPassword(cemailValue, cpasswordValue).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
             @Override
             public void onComplete(@NonNull Task<AuthResult> task) {
                 if (task.isSuccessful()) {
                     checkCompanyExist();
                 } else {
                     Toast.makeText(LoginC.this, "Error Login", Toast.LENGTH_LONG).show();
                 }
             }
         });

     }

    }

    private void checkCompanyExist() {
        final String user_id = mAuth.getCurrentUser().getUid();
        mRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(user_id)){
                    Intent intent = new Intent(LoginC.this, CompanyTimeline.class);
                    startActivity(intent);

            } else{
                Toast.makeText(LoginC.this, "Error hai", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}








