package com.pontiff.jaagir;

public class CompanyList{
    String companyId;
    String companyName;
    String companyProfile;
    String companyAddress;
    String companyEmail;
    String companyPhone;
    String companyPassword;
    String companyConfirmpassword;

    public CompanyList(){


    }


    public CompanyList(String companyId, String companyName, String companyProfile, String companyAddress, String companyEmail, String companyPhone, String companyPassword, String companyConfirmpassword) {
        this.companyId = companyId;
        this.companyName = companyName;
        this.companyProfile = companyProfile;
        this.companyAddress = companyAddress;
        this.companyEmail = companyEmail;
        this.companyPhone = companyPhone;
        this.companyPassword = companyPassword;
        this.companyConfirmpassword = companyConfirmpassword;
    }

    public String getCompanyId() {

        return companyId;
    }

   public String getCompanyName()
   {
       return companyName;
    }

    public String getCompanyProfile(){
        return companyProfile;
    }

    public String getCompanyAddress(){
        return companyAddress;

    }

    public String getCompanyEmail(){
        return companyEmail;
    }

    public String getCompanyPhone(){
        return companyPhone;
    }
    public String getCompanyPassword(){
        return companyPassword;
    }
    public String getCompanyConfrimpassword(){
        return companyConfirmpassword;
    }
}