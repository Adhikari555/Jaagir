package com.pontiff.jaagir;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class TestingAgain{
    private String email;
    private String password;

    public  TestingAgain(){

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
