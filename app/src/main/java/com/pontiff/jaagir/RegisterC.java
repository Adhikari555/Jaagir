package com.pontiff.jaagir;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class RegisterC extends AppCompatActivity {
    EditText CompanyName, CompanyAddress, CompanyEmail, CompanyPhone, CompanyPassword, CompanyConfirmPassword;
    Button Register;
    Spinner SpinnerCompany;

    private DatabaseReference mRef;
    private FirebaseAuth mAuth;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_c);
        Firebase.setAndroidContext(this);

        mAuth = FirebaseAuth.getInstance();
       mRef = FirebaseDatabase.getInstance().getReference().child("CompanyList");




        CompanyName = (EditText) findViewById(R.id.companyname);
        SpinnerCompany = (Spinner) findViewById(R.id.spinner);
        CompanyAddress = (EditText) findViewById(R.id.companyaddress);
        CompanyEmail = (EditText) findViewById(R.id.companyemail);
        CompanyPhone = (EditText) findViewById(R.id.companyphone);
        CompanyPassword = (EditText) findViewById(R.id.companypassword);
        CompanyConfirmPassword = (EditText) findViewById(R.id.companyconfirmpassword);
        Register = (Button) findViewById(R.id.companyregister);


        Register.setOnClickListener(new View.OnClickListener() {
            @Override


            public void onClick(View view) {


                addCompanyList();

            }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.



        }











    private void addCompanyList() {
        final String cname = CompanyName.getText().toString().trim();
        final String cspinner = SpinnerCompany.getSelectedItem().toString();
        final String caddress = CompanyAddress.getText().toString().trim();
        final String cemail = CompanyEmail.getText().toString().trim();
        final String cphone = CompanyPhone.getText().toString().trim();
        final String cpassword = CompanyPassword.getText().toString().trim();
        final String cconfirmpasword = CompanyConfirmPassword.getText().toString().trim();

        String id = mRef.push().getKey();
        CompanyList companyList = new CompanyList(id, cname, cspinner, caddress, cemail, cphone, cpassword, cconfirmpasword);
        mRef.child(id).setValue(companyList);
        mAuth.createUserWithEmailAndPassword(cemail,cpassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()){
                    String user_id = mAuth.getCurrentUser().getUid();
                    DatabaseReference current_company_db = mRef.child(user_id);
                    current_company_db.child("companyName").setValue(cname);
                    current_company_db.child("companyProfile").setValue(cspinner);
                    current_company_db.child("companyAddress").setValue(caddress);
                    current_company_db.child("companyEmail").setValue(cemail);
                    current_company_db.child("companyPhone").setValue(cphone);
                    current_company_db.child("companyPassword").setValue(cpassword);
                    current_company_db.child("companyConfirmPassword").setValue(cconfirmpasword);

                    Intent intent = new Intent(RegisterC.this, CompanyTimeline.class);
                    startActivity(intent);
                }
            }
        });



        Toast.makeText(this, "company added", Toast.LENGTH_LONG).show();



        // [START create_user_with_email]


    }
}



